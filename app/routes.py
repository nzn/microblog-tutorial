# Home page route; Use render\_template() function
# Fake posts in view function
# Login view function

from flask import render_template, flash, redirect, url_for

from app import app
from app.forms import LoginForm

@app.route('/')
@app.route('/index')
def index():
    user = {'username': 'Miguel'}
    posts = [
	{
		'author': {'username': 'John'},
		'body': 'Beautiful day in Portland!'
	},
	{
		'author': {'username': 'Susan'},
		'body': 'The Avengers movie was so cool!'
	},
	{
		'author': {'username': 'Negili'},
		'body': 'Bakhi janam knn!'
	},
	{
		'author': {'username': 'Negili'},
		'body': 'Salam chinkosooooooooooo!'
	}
    ]   	
	
    return render_template('index.html', title='Home Page', user=user, posts=posts)
    #return render_template('index.html', user=user)

@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        flash('Login requested for user {}, remember_me={}'.format(
            form.username.data, form.remember_me.data))
        return redirect(url_for('index'))
    return render_template('login.html', title='Sign In', form=form)

